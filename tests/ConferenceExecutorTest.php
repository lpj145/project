<?php
declare(strict_types=1);

namespace AppTest;

use App\ConferenceExecutor;
use App\ConferenceTime;
use App\Events\ConstraintEvent;
use App\Events\TalkEvent;
use App\Exceptions\TimeFullException;
use App\Exceptions\TimeoutException;
use PHPUnit\Framework\TestCase;

class ConferenceExecutorTest extends TestCase
{
    public function testRun()
    {
        $conference = $this->setupConference();
        (new ConferenceExecutor())
            ->runConference($conference);
        $this->assertTrue(true, 'Conference executed success.');
    }

    protected function setupConference(): ConferenceTime
    {
        $conference = ConferenceTime::factory(9, 17);
        $conference->addConstraintEvent(12, ConstraintEvent::factory(60, 'Test pause.'));
        $conference->addConstraintEvent(17, ConstraintEvent::factory(60, 'Test pause.'));
        try {
            for ($i = 0; $i < 14; $i++) {
                $conference->addEvent(TalkEvent::factory(30, 'Test talk event: '));
            }
        } catch (TimeFullException $exception) {
        }
        return $conference;
    }
}
