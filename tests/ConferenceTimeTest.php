<?php
declare(strict_types=1);

namespace AppTest;

use App\ConferenceTime;
use App\Events\ConstraintEvent;
use App\Events\TalkEvent;
use App\Exceptions\InputException;
use App\Exceptions\TimeFullException;
use App\Exceptions\TimeoutException;
use PHPUnit\Framework\TestCase;

class ConferenceTimeTest extends TestCase
{
    /**
     * @var ConferenceTime
     */
    protected $conference;
    protected function setUp(): void
    {
        parent::setUp();
        $this->conference = ConferenceTime::factory(9, 17);
    }

    public function testGetConferenceTimes()
    {
        $this->assertCount(9, $this->conference->getConferenceTimes());
    }

    public function testAddEventWithoutProblem()
    {
        $conference = ConferenceTime::factory(9, 17);
        $conference->addEvent(TalkEvent::factory(2, 'Test best event.'));
        $this->assertTrue(true, 'can add event without problem.');
    }

    public function testAllocateEightEvents()
    {
        $conference = ConferenceTime::factory(9, 17);
        for ($i = 0; $i < 9; $i++) {
            $conference->addEvent(TalkEvent::factory(60, 'Based on eight events.'));
        }
        $this->assertTrue(true, 'All events can be added');
    }

    public function testCantAllocateLongEvent()
    {
        $this->expectException(TimeoutException::class);
        ConferenceTime::factory(9, 17)
            ->addEvent(TalkEvent::factory(720, 'Invalid Event'));
    }

    public function testGetConstraints()
    {
        $this->conference->addConstraintEvent(14, ConstraintEvent::factory(30, 'Pause to lunch.'));
        $this->assertCount(1, $this->conference->getConstraints());
    }

    public function testAddConstraintEvent()
    {
        $this->conference->addConstraintEvent(16, ConstraintEvent::factory(30, 'Long breakfast.'));
        $this->assertEquals(true, true, 'Constraint can be added.');
    }

    /**
     * Test in same exact hour two constraints with compatible times.
     */
    public function testDoubleConstraintInSameTime()
    {
        $exactHour = 15;
        $this->conference->addConstraintEvent($exactHour, ConstraintEvent::factory(30, 'Pause to breakfast.'));
        $this->conference->addConstraintEvent($exactHour, ConstraintEvent::factory(30, 'Pause to announcements'));
        $this->assertCount(2, $this->conference->getConstraints()[$exactHour]);
    }

    /**
     * Test in same exact hour two constraints with incompatibles time.
     */
    public function testExceptionDoubledConstraintInSameTime()
    {
        $exactHour = 15;
        $this->expectException(InputException::class);
        $this->conference->addConstraintEvent($exactHour, ConstraintEvent::factory(42, 'Pause to breakfast.'));
        $this->conference->addConstraintEvent($exactHour, ConstraintEvent::factory(30, 'Pause to announcements'));
    }

    public function testCountEvents()
    {
        $this->conference->addConstraintEvent(10, ConstraintEvent::factory(10, 'Long breakfast.'));
        $this->assertEquals(
            1,
            $this->conference->countConstrains(),
            'Conference have one event'
        );
    }
}
