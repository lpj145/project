<?php

require __DIR__.'/vendor/autoload.php';

$conference = (new \App\ConferenceCli())
    ->execute();

(new \App\ConferenceExecutor())
    ->executeCli($conference);