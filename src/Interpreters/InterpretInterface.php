<?php
namespace App\Interpreters;

use App\Events\EventInterface;

interface InterpretInterface
{
    public function interpret(string $message): EventInterface;
}