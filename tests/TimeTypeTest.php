<?php
declare(strict_types=1);

namespace AppTest;

use App\Events\ConstraintEvent;
use App\TimeType;
use PHPUnit\Framework\TestCase;

class TimeTypeTest extends TestCase
{

    public function testFactory()
    {
        $this->assertInstanceOf(
            TimeType::class,
            TimeType::factory(12)
        );
    }

    public function testHaveTimeToEvent()
    {
        $event = ConstraintEvent::factory(10, 'Time to lunch.');
        $this->assertEquals(
            true,
            (TimeType::factory(12))
                ->haveTimeToEvent($event)
        );
    }

    public function testAddEvent()
    {
        $event = ConstraintEvent::factory(10, 'Time to event.');
        $timeType = TimeType::factory(10);
        $this->assertInstanceOf(TimeType::class, $timeType->addEvent($event));
        $this->assertEquals(1, $timeType->count());
    }
}
