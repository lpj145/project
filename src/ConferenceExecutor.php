<?php
declare(strict_types=1);

namespace App;


use App\Events\ConstraintEvent;

class ConferenceExecutor extends CliBase
{
    private $currentEvent;

    public function executeCli(ConferenceTime $conferenceTime)
    {
        if (!$this->askConfirm('Ok, vamos iniciar a conferência ?')) {
            return;
        }
        $this->writeMessage(sprintf('Hoje nós teremos %d talks, aproveite.', $conferenceTime->countEvents()));
        $this->runConference($conferenceTime);
        $this->writeMessage('Fim da conferência, obrigado!');
    }

    public function runConference(ConferenceTime $conferenceTime)
    {
        $events = $conferenceTime->getConferenceTimes();
        array_map(function(TimeType $time){
            $this->executeEvent($time);
        }, $events);
    }

    protected function executeEvent(TimeType $time)
    {
        while ($time->hasNextEvent()) {
            $hourOfEvent = $time->currentTwelveClockTime();
            $event = $time->getNextEvent();

            if ($this->currentEvent === $event) {
                continue;
            }

            $this->currentEvent = $event;
            if ($event instanceof ConstraintEvent) {
                $this->writeMessage(sprintf('%s %s', $hourOfEvent, $event->getDescription()));
                continue;
            }

            $this->writeMessage(
                sprintf(
                    '%s %s %dmin',
                    $hourOfEvent,
                    $event->getDescription(),
                    $event->getDuration()
                )
            );
        }
    }
}