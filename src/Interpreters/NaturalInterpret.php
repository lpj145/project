<?php
declare(strict_types=1);

namespace App\Interpreters;


use App\Events\EventInterface;
use App\Events\TalkEvent;
use App\Exceptions\NaturalInterpretException;

class NaturalInterpret implements InterpretInterface
{
    public function interpret(string $message): EventInterface
    {
        $minuteTerm = $this->findTerm($message, '\d+min');
        $lightTerm = $this->findTerm($message, 'lightning');

        if (is_null($minuteTerm) && is_null($lightTerm)) {
            throw new NaturalInterpretException('Não consigo entender o que você quer, pode repetir ?');
        }

        if (!is_null($minuteTerm) && !is_null($lightTerm)) {
            throw new NaturalInterpretException('Com dois tempos não da certo :)');
        }

        $timeTerm = $minuteTerm ?? $lightTerm;
        $message = str_replace($timeTerm, '', $message);

        return $minuteTerm ? TalkEvent::factory((int)$minuteTerm, $message)
            : TalkEvent::factoryFromLightning($message);
    }

    public static function factory(): InterpretInterface
    {
        return new static();
    }

    protected function findTerm(string $message, string $term)
    {
        preg_match_all("/{$term}/", $message, $match);
        $match = $match[0] ?? [];
        if (count($match) > 1) {
            throw new NaturalInterpretException('Há muitas definições de tempo, uma talk só tem um tempo.');
        }

        return $match[0] ?? null;
    }
}