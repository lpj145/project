<?php
namespace App\Events;


interface EventInterface
{
    public function getMinutes(): int;
    public function getDuration(): int;
    public function getDescription(): string;
    public function reduceTime(int $time): void;
}