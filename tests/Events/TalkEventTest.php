<?php
declare(strict_types=1);

namespace AppTest\Events;

use App\Events\TalkEvent;
use App\Exceptions\InputException;
use PHPUnit\Framework\TestCase;

class TalkEventTest extends TestCase
{
    public function testGetDescription()
    {
        $talk = TalkEvent::factory(10, 'My event title');
        $this->assertEquals('My event title', $talk->getDescription());
    }

    public function testFactory()
    {
        $this->assertInstanceOf(
            TalkEvent::class,
            TalkEvent::factory(10, 'Event description')
        );
    }

    public function testEventWithNumberInTitle()
    {
        $this->expectException(InputException::class);
        TalkEvent::factory(60, 'Uma talk com 43 motivos.');
    }

    public function testInvalidFactory()
    {
        $this->expectException(InputException::class);
        // Talk need to have time and a good description
        TalkEvent::factory(0, '');
    }

    public function testReduceTime()
    {
        $talk = TalkEvent::factory(60, 'Event description');
        $talk->reduceTime(30);
        $this->assertEquals(
            30,
            $talk->getMinutes()
        );
    }

    public function testGetMinutes()
    {
        $talk = TalkEvent::factory(30, 'Event description');
        $this->assertEquals(30, $talk->getMinutes());
    }
}
