<?php
declare(strict_types=1);

namespace App\Events;


use App\Exceptions\InputException;

class TalkEvent implements EventInterface
{
    const LIGHTNING_TALK_TIME = 5;

    /**
     * @var int
     */
    private $time;
    /**
     * @var int
     */
    private $duration;
    /**
     * @var string
     */
    private $description;

    private function __construct(int $time, string $description)
    {
        $this->time = $time;
        $this->duration = $time;
        $this->description = $description;
    }

    public function getMinutes(): int
    {
        return $this->time;
    }

    public function getDuration(): int
    {
        return $this->duration;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function reduceTime(int $time): void
    {
        if ($this->time < $time) {
            throw new InputException('O tempo desse evento não pode ser reduzido pra negativo');
        }
        $this->time -= $time;
    }

    public static function factory(int $time, string $description)
    {
        if ($time <= 0) {
            throw new InputException(
                'Porque uma talk que não tem duração ?'
            );
        }

        if (strlen($description) < 6) {
            throw new InputException(
                'As pessoas vão se interessar mais por uma talk com uma descrição clara.'
            );
        }

        preg_match('/\d+/', $description, $match);
        if (count($match) > 0) {
            throw new InputException('Uma talk não pode ter números, é uma regra.');
        }

        return new static($time, $description);
    }

    public static function factoryFromLightning(string $description)
    {
        return self::factory(self::LIGHTNING_TALK_TIME, $description);
    }
}