<?php
declare(strict_types=1);

namespace App\Events;


use App\Exceptions\InputException;

class ConstraintEvent implements EventInterface
{
    /**
     * @var int
     */
    private $minutes;
    /**
     * @var int
     */
    private $duration;
    /**
     * @var string
     */
    private $description;

    private function __construct(int $minutes, string $description)
    {
        $this->minutes = $minutes;
        $this->duration = $minutes;
        $this->description = $description;
    }

    public function getMinutes(): int
    {
        return $this->minutes;
    }

    public function getDuration(): int
    {
        return $this->duration;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function reduceTime(int $time): void
    {
        return;
    }

    public static function factory(int $time, string $description): ConstraintEvent
    {
        if ($time <= 0) {
            throw new InputException(
                'Porque uma pausa que não vai ter duração ?'
            );
        }

        if (strlen($description) < 6) {
            throw new InputException(
                'Pessoas não conseguem entender pausas sem motivos.'
            );
        }

        return new static($time, $description);
    }
}