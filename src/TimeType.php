<?php
declare(strict_types=1);

namespace App;


use App\Events\EventInterface;
use App\Exceptions\InputException;

class TimeType
{
    const ONE_HOUR_MINUTES = 60;

    private $pointersToEvents = [];

    private $consumedTime = 0;

    private $events = [];
    /**
     * @var int
     */
    private $dayTime;

    private function __construct(int $dayTime)
    {
        $this->dayTime = $dayTime;
    }

    /**
     * @return int
     */
    public function getDayTime(): int
    {
        return $this->dayTime;
    }

    public function getNextEvent(): EventInterface
    {
        $this->consumedTime += array_shift($this->pointersToEvents);
        return array_shift($this->events);
    }

    public function hasNextEvent(): bool
    {
        return !empty($this->events);
    }

    public function count(): int
    {
        return count($this->pointersToEvents);
    }

    public function addEvent(EventInterface $event)
    {
        $remainingTime = $this->getRemainingTime();

        $eventTime = $event->getMinutes();
        $timeToReduce = $eventTime;

        if ($eventTime > $remainingTime) {
            $timeToReduce = $remainingTime;
        }

        $event->reduceTime($timeToReduce);
        $this->events[] = $event;
        $this->pointersToEvents[] = $timeToReduce;
        return $this;
    }

    public function haveTimeToEvent(EventInterface $event): bool
    {
        return $this->getRemainingTime() >= $event->getMinutes();
    }

    public function myTimeIsFull(): bool
    {
        return $this->sumTimeOfEvents() === self::ONE_HOUR_MINUTES;
    }

    public function currentTwelveClockTime()
    {
        $eventTime = str_pad((string)$this->consumedTime, 2, '0', STR_PAD_LEFT);
        $stringTime = sprintf('%d:%s', $this->dayTime, $eventTime);
        $time = date_create_from_format('G:i', $stringTime);
        return $time->format('H:i A');
    }

    /**
     * Time is guide by representation constant.
     * @return float|int
     */
    public function getRemainingTime()
    {
        return self::ONE_HOUR_MINUTES - $this->sumTimeOfEvents();
    }

    public function sumTimeOfEvents()
    {
        return array_sum($this->pointersToEvents);
    }

    public static function factory(int $dayTime)
    {
        return new static($dayTime);
    }
}