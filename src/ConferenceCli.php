<?php
declare(strict_types=1);

namespace App;

use App\Events\ConstraintEvent;
use App\Events\EventInterface;
use App\Exceptions\InputException;
use App\Exceptions\NaturalInterpretException;
use App\Exceptions\TimeFullException;
use App\Exceptions\TimeoutException;
use App\Interpreters\NaturalInterpret;

class ConferenceCli extends CliBase
{
    const START_CONFERENCE_TIME = 9;
    const END_CONFERENCE_TIME = 17;
    const LUNCH_DURATION = 60;
    const LUNCH_TIME = 12;
    const NETWORK_EVENT_TIME = 17;
    const NETWORKING_DURATION = 60;

    public function execute()
    {
        $this->writeMessage('Bem vindo a maior conferência de programação do ano.');
        $conference = $this->initConference();
        $this->configureBreakFast($conference);
        $this->configureNetworkEvent($conference);
        return $this->askForTalks($conference);
    }

    protected function askForTalks(ConferenceTime $conferenceTime): ConferenceTime
    {
        $this->writeMessage('Chegou a hora de configurar as talks do dia.');
        do {
            try {
                $event = $this->interpretMessage();
            } catch (NaturalInterpretException $exception) {
                $this->writeMessage($exception->getMessage());
                continue;
            } catch (InputException $exception) {
                $this->writeMessage($exception->getMessage());
                continue;
            }

            try {
                $conferenceTime->addEvent($event);
            } catch (TimeoutException $exception) {
                $this->writeMessage('Poxa, não da tempo pra essa talk nessa conferência não.');
                $this->writeMessage(sprintf('O tempo restante é de %d minutos.', $exception->getRemainingTime()));
            }
        } while($conferenceTime->haveAvailableTime());
        return $conferenceTime;
    }

    protected function configureBreakFast(ConferenceTime $conferenceTime)
    {
        $this->writeMessage(
            sprintf('Configurando lanche para as %s horas.', self::LUNCH_TIME)
        );
        return $conferenceTime->addConstraintEvent(
            self::LUNCH_TIME,
            ConstraintEvent::factory(self::LUNCH_DURATION, 'Pausa para o almoço.')
        );
    }

    protected function configureNetworkEvent(ConferenceTime $conferenceTime)
    {
        $this->writeMessage(
            sprintf('Configurando networking para as %s horas.', self::NETWORK_EVENT_TIME)
        );
        return $conferenceTime->addConstraintEvent(
            self::NETWORK_EVENT_TIME,
            ConstraintEvent::factory(self::NETWORKING_DURATION, 'É hora de conversar!')
        );
    }

    protected function initConference(): ConferenceTime
    {
        $this->writeMessage(sprintf('Configurando inicio da conferência para as %s horas.', self::START_CONFERENCE_TIME));
        $this->writeMessage(sprintf('Configurando fim da conferência para as %s horas.', self::END_CONFERENCE_TIME));
        return ConferenceTime::factory(
            self::START_CONFERENCE_TIME,
            self::END_CONFERENCE_TIME
        );
    }

    protected function interpretMessage(): EventInterface
    {
        $this->writeMessage('Descreva a talk seguido do tempo em minutos ou simples lightning.');
        $message = $this->askQuestion(':> ');
        return NaturalInterpret::factory()
            ->interpret($message);
    }
}