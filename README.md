#### Track - Project
Primeiramente, obrigado, obrigado pelas conversas e obrigado por dar a oportunidade poder participar do teste.

Arquitetura sempre é um problema sério, e pensando em todos os pontos levantados, e no que
consegui compreender, eu tentei fazer o melhor.

#### Track Manager
Eu escolhi o desafio, porque é um desafio pra mim desde a muito, sempre pensei em que algoritimos seriam interessantes
para se controlar o tempo, a hora, o minuto, então escolhi ele.

Os pontos levantados consistem de: objetivo primário, controlar uma **conferência** controlar os tempos e eventos da conferências,
bem como por meio de linguagem natual, interpretar a criação desses eventos, sejam eles maleavéis ou não, então por ordem temos:

``TimeType`` - Não é obsessão por tipos, mas já que o tempo é um dos pilares de nossa aplicação, ele merece virar um tempo.

``ConferenceTime`` - Controla o tempo de uma conferência, ele é como uma agenda, possuí os estados: (tempo e eventos).

``ConferenceExecutor`` - Controla a execução de uma conferência.

``NaturalInterpret``  - Interpreta por um canal simples, strings, como um evento (talk) pode ser construído.

``EventInterface`` - Eventos baseado nesse cara, podem ser implementados em qualquer tempo, qualquer conferência.

#### Fundamentos
Nos códigos você vai encontrar a mistura dos 3 paradigmas, ``funcional`` ``OO`` e ``imperativo`` na pratica,
o funcional é o mais interessante de todos, porém o PHP tem limites que preciso respeitar.

O ponto de entrada é bem simples, tudo começa no ``ConferenceCli`` que vai configurações na conferência de eventos ``ConstraintEvent``
e depois perguntas de estilo natural para ser interpretado por ``NaturalInterpret`` depois o fluxo é passado para ``ConferenceExecutor``
que é o cara responsável para executar a conferência, ele quem vai entender quem é da vez e o que fazer, tanto ele quanto ``ConferenceCli``
usam de ``CliBase`` para fazer controle do STDIN e STDOUT.

Pontos para crescimento, é até fácil de fazer um random nas talks, porque tudo se baseia na conferência e no tempo que ela tem, também,
é fácil, não interpretar eventos apenas por ser humano, mas desenvolver um novo interpretador que possa construir eventos, algo que possa vir de api,
ou de um robo executando via cli, bem acho que é isso, se pareci confuso, tento me explicar melhor.

#### Execução
No arquivo app.php você consegue encontrar a inteira implementação que o desafio propõe, onde você pode digitar, e ver mensagens.
````bash
php app.php
````

#### Tests
Os testes foram feitos com a ajuda do PHPUnit 9+, sim, eu amo PHP.
````bash
composer run tests
````

Além dos testes unitários existe um pequeno arquivo php, que contém um teste prático, com variações de tempos mais complexas que a
proposta pelo input dos desafios, afinal o tempo engana muita gente :)
````bash
php test.php
````