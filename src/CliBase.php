<?php
declare(strict_types=1);

namespace App;


class CliBase
{
    protected function writeMessage(string $message)
    {
        echo $message.PHP_EOL;
    }

    protected function abort(string $message)
    {
        $this->writeMessage($message);
        exit(0);
    }

    /**
     * I need it ?
     * @param string $message
     * @param int|null $default
     * @return int
     */
    protected function askNumeric(string $message, ?int $default = null): int
    {
        $value = '';
        while(true) {
            $value = readline($message." ({$default})? ");
            if (empty($value)) {
                $value = $default;
                break;
            }
            if (is_numeric($value)) {
                break;
            }
        }
        return (int)$value;
    }

    protected function askQuestion(string $message, bool $removeSpaces = false)
    {
        $value = readline($message);

        if ($removeSpaces) {
            $value = trim($value);
        }

        return $value;
    }

    protected function askAcceptable(string $message, array $acceptable, string $default = '')
    {
        $message = sprintf('%s [%s]', $message, implode(', ', $acceptable));
        $input = '';
        while (true) {
            $input = $this->askQuestion($message);
            if (empty($input)) {
                $input = $default;
                break;
            }
            if (in_array($input, $acceptable)) {
                break;
            }
        }
        return $input;
    }

    protected function askConfirm(string $message): bool
    {
        return $this->askAcceptable($message, ['Y', 'n'], 'Y') === 'Y';
    }
}