<?php
declare(strict_types=1);

namespace AppTest\Interpreters;

use App\Events\TalkEvent;
use App\Exceptions\NaturalInterpretException;
use App\Interpreters\NaturalInterpret;
use PHPUnit\Framework\TestCase;

class NaturalInterpretTest extends TestCase
{
    public function testInterpret()
    {
        $interpret = NaturalInterpret::factory();
        $this->assertInstanceOf(
            TalkEvent::class,
            $interpret->interpret('This is a lendary talk 65min.')
        );
    }

    public function testMuchNoise()
    {
        $this->expectException(NaturalInterpretException::class);
        (NaturalInterpret::factory())
            ->interpret('this is a talk with invalid times 65min 67min 900min');
    }

    public function testLotTimes()
    {
        $this->expectException(NaturalInterpretException::class);
        (NaturalInterpret::factory())
            ->interpret('this is a talk with invalid times lightning lightning');
    }
}
