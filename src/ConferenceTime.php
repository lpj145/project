<?php
declare(strict_types=1);

namespace App;


use App\Events\EventInterface;
use App\Exceptions\InputException;
use App\Exceptions\TimeFullException;
use App\Exceptions\TimeoutException;

class ConferenceTime
{
    const SMALLEST_TIME = 2;
    /**
     * @var int
     */
    private $startHour;
    /**
     * @var int
     */
    private $endHour;
    /**
     * @var TimeType[]
     */
    private $conferenceTimes;

    private $constraints = [];

    private $eventsQuantity = 0;

    private function __construct(int $startHour, int $endHour)
    {
        $this->startHour = $startHour;
        $this->endHour = $endHour;
        $this->conferenceTimes = array_reduce(range($startHour, $endHour), function($hours, $hour){
            $hours[$hour] = TimeType::factory($hour);
            return $hours;
        }, []);
    }

    public function addConstraintEvent(int $exactHour, EventInterface $event): ConferenceTime
    {
        if (!array_key_exists($exactHour, $this->conferenceTimes)) {
            throw new InputException('A não! você esta colocando uma pausa num momento inexistente.');
        }
        $conferenceTime = $this->conferenceTimes[$exactHour];

        if (!$conferenceTime->haveTimeToEvent($event)) {
            throw new InputException("Sem tempo suficiente para o evento: {$event->getDescription()}");
        }

        try {
            $conferenceTime->addEvent($event);
            $this->constraints[$exactHour][] = $event;
        } catch (InputException $exception) {
            throw new InputException('Não há tempo suficiente para esta pausa nessa hora.');
        }

        return $this;
    }

    public function addEvent(EventInterface $event): ConferenceTime
    {
        $this->verifyIfHaveTimeToEvent($event);
        while($event->getMinutes() > 0) {
            $this->getNextFreeTime()
                ->addEvent($event);
        }

        $this->eventsQuantity += 1;
        return $this;
    }

    public function haveAvailableTime(): bool
    {
        return $this->getNextFreeTime() instanceof TimeType;
    }

    /**
     * @return TimeType[]
     */
    public function getConferenceTimes(): array
    {
        return $this->conferenceTimes;
    }

    public function countEvents(): int
    {
        return $this->eventsQuantity;
    }

    public function getConstraints(): array
    {
        return $this->constraints;
    }

    public function countConstrains(): int
    {
        return count($this->constraints);
    }

    public function getStartHour(): int
    {
        return $this->startHour;
    }

    public function getEndHour(): int
    {
        return $this->endHour;
    }

    protected function getNextFreeTime(): ?TimeType
    {
        $pointer = $this->startHour;
        $time = $this->conferenceTimes[$pointer];

        if (!$time->myTimeIsFull()) {
            return $time;
        }

        while(true) {
            if ($pointer === $this->endHour) {
                $time = null;
                break;
            }

            $pointer += 1;
            $time = $this->conferenceTimes[$pointer];
            if (!$time->myTimeIsFull()) {
                break;
            }
        }
        return $time;
    }

    protected function verifyIfHaveTimeToEvent(EventInterface $event)
    {
        $availableTime = 0;
        foreach ($this->conferenceTimes as $conferenceTime) {
            $availableTime += $conferenceTime->getRemainingTime();

            if ($availableTime >= $event->getDuration()) {
                return;
            }
        }
        throw new TimeoutException('Não há tempo disponível para essa talk', $availableTime);
    }

    public static function factory(int $startHour, int $endHour): ConferenceTime
    {
        if ($startHour > $endHour || ($endHour - $startHour) < self::SMALLEST_TIME) {
            throw new InputException(
                'A não, como é possível realizar uma conferência sem tempo, você precisa de no minímo duas horas ?'
            );
        }
        return new static($startHour, $endHour);
    }
}