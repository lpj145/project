<?php

require __DIR__.'/vendor/autoload.php';

$conference = \App\ConferenceTime::factory(9, 17);

$conference
    ->addConstraintEvent(12, \App\Events\ConstraintEvent::factory(60, 'Lanche'))
    ->addConstraintEvent(17, \App\Events\ConstraintEvent::factory(60, 'Networking'));

$conference
    ->addEvent(\App\Events\TalkEvent::factory(45, 'Evento de talk'))
    ->addEvent(\App\Events\TalkEvent::factory(75, 'Evento de talk'))
    ->addEvent(\App\Events\TalkEvent::factory(60, 'Evento de talk'))
    ->addEvent(\App\Events\TalkEvent::factory(30, 'Evento de talk'))
    ->addEvent(\App\Events\TalkEvent::factory(60, 'Evento de talk'))
    ->addEvent(\App\Events\TalkEvent::factory(90, 'Evento de talk'))
    ->addEvent(\App\Events\TalkEvent::factory(60, 'Evento de talk'));


(new \App\ConferenceExecutor())
    ->runConference($conference);