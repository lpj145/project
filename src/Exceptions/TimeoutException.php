<?php
declare(strict_types=1);

namespace App\Exceptions;


class TimeoutException extends \InvalidArgumentException
{
    /**
     * @var int
     */
    private $remainTime;

    public function __construct(string $message = "", int $remainTime)
    {
        parent::__construct($message);
        $this->remainTime = $remainTime;
    }

    public function getRemainingTime()
    {
        return $this->remainTime;
    }
}